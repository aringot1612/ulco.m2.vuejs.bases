var container = undefined;
var txt1 = undefined;
var txt2 = undefined;
var txt3 = undefined;
var txt4 = undefined;
var initText = undefined;

document.addEventListener('DOMContentLoaded', function(){
    container = document.getElementById("container")
    txt1 = document.getElementById("text1");
    txt2 = document.getElementById("text2");
    txt3 = document.getElementById("text3");
    txt4 = document.getElementById("text4");
    container.style.display = "none";

    function onChange(value){
        if(value != "")
            container.style.display = ""
        else
            container.style.display = "none";

        txt1.innerHTML = value;
        txt2.innerHTML = value;
        txt3.innerHTML = value;
        txt4.innerHTML = value;
    }
})